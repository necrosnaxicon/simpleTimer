## Simple Timer

Ultra simple timer made in [Svelte](https://svelte.dev/).

### Goal:
Create a timer for recording contraction duration, and so we can tell when to go to the hospital for delivery.

### To do:
- Look into using indexedDB instead of localstorage
- Parse seconds on long times correctly
- Sort times properly
- Delete data button
- Refactor code
- Add tabs so multiple timers can go at once

