import { opts } from "./constants";

/**
 * Calculate our metrics from out time array for visualization
 * @param { array } timesArray
 */
export default function calculateMetrics(timesArray) {
  const millisPerHour = 3600000;
  const latestTime = timesArray[timesArray.length - 1][1];
  const earliestPossible = latestTime - millisPerHour;
  const timeBetween = [0];

  /**
   * Get all contractions in the last hour:
   */
  const dataArr = timesArray.filter(time => Number(time[1]) > earliestPossible);

  /**
   * Get the time distance between contractions
   */
  dataArr.some((item, i) => {
    if (i + 1 > dataArr.length - 1) {
      return;
    }
    let dur = dataArr[i + 1][0] - item[1];
    timeBetween.push(dur);
  });

  /**
   * Create a date object to plot
   */
  const plotArr = dataArr.map((item, i) => {
    return {
      start: toLocaleTimeString(Number(item[0]), opts),
      end: toLocaleTimeString(Number(item[1]),opts),
      duration: (Math.round(((Number(item[1]) - Number(item[0]))/1000)*100)/100),
      space: timeBetween[i] ? Number(timeBetween[i]) : null,
    };
  });
  console.log(plotArr);

  return {
    'plotData': plotArr,
  }
}

/**
   * Convert numbers to date localeTimeString
   * @param { Number } timeNum
   * @param { Object } opts
   * @returns { String }
   */
function toLocaleTimeString(timeNum, opts) {
  return new Date(timeNum).toLocaleTimeString("en-US", opts)
}
