import { errors } from "./stores";
import { saved } from "./stores";

/**
 * Check if local storage is available on mount
 * @returns {void}
 */
export function localStorageExists() {
  try {
    const x = "__storage_test__";
    localStorage.setItem(x, x);
    localStorage.removeItem(x);
  } catch (e) {
    const err = {
        error: "Local Storage Not Available",
        message: e.name
    }
    errors.setError(err);
  }
}

/**
 * Write our durations into local storage
 * @param {number} startTime
 * @param {number} endTime
 * @returns {void}
 */

export function storeTime(startTime, endTime) {
    /**
     * Start time
     * @type {string}
     */
    const key = `${startTime}`;
    /**
     * End time
     * @type {string}
     */
    const val = `${endTime}`;
    localStorage.setItem(key, val);
}

/**
 * Get values from local storage:
 * @returns {array}
 */
export function getTimes() {
  // @ts-ignore
  return Object.entries(localStorage).sort((a,b)=> a[0] - b[0]);
}

/**
 * Clear values in local storage:
 * @returns {void}
 */
export function clearStorage() {
  localStorage.clear();
  saved.setFalse();
}
