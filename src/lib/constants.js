  /**
   * Options for the LocalTimeString display
   * @type { object }
   */
  const opts = { hour: "2-digit", minute: "2-digit" };

  export { opts }
