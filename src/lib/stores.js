import { writable } from "svelte/store";

function toggleDisable() {
  const { subscribe, update } = writable(true);

  return {
    subscribe,
    toggle: () => update((val) => !val),
  };
}

function setSaved() {
  const { subscribe, update } = writable(true);

  return {
    subscribe,
    setFalse: () => update((val) => false),
    setTrue: () => update((val) => true)
  };
}

function setError() {
  const { subscribe, update } = writable({ error: "", message: "" });

  return {
    subscribe,
    setError: (err) => update((val) => err),
  };
}

export const disabled = toggleDisable();
export const seconds = writable(0);
export const minutes = writable(0);
export const errors = setError();
export const saved = setSaved();
